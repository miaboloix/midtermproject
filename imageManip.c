// contains all the implementations of the functions for aditing the ppm files... like swap, cloackout,etc
//

#include <stdlib.h>
#include <stdio.h>
#include "ppmIO.h"
#include "commandUtil.h"
#include "imageManip.h"

//blackout function - emily
int blackout(Image* img) {
  int nRows = img->rows;
  int nCols = img->cols;
  int blackRows = (nRows - (nRows % 2))/2;
  int blackCols = (nCols - (nCols % 2))/2;
  //  int nPixels = nRows*nCols; 
  for(int rows = 0; rows < nRows; rows++) {
    for(int cols = 0; cols < nCols; cols++) {
      if(rows < blackRows && cols > blackCols) {
	img->pixels[rows*nCols+cols].r = 0;
	img->pixels[rows*nCols+cols].b = 0;
	img->pixels[rows*nCols+cols].g = 0;
      }
    }
  }
  return 0; 
}

//swap function - mia
int swap(Image * img) {
  int ncols = img->cols;
  int nrows = img->rows;
  for (int rows = 0; rows < nrows; rows++) {
    for (int cols = 0; cols < ncols; cols++) {
      char red = img->pixels[rows*ncols+cols].r;
      char green = img->pixels[rows*ncols+cols].g;
      char blue = img->pixels[rows*ncols+cols].b;
      img->pixels[rows*ncols+cols].r = green;
      img->pixels[rows*ncols+cols].b = red;
      img->pixels[rows*ncols+cols].g = blue;
    }
  }
  return 0;
}

//contrast function - mia
int contrast(Image *img, double mult) {
  int ncols = img->cols;
  int nrows = img->rows;
  for (int rows = 0; rows < nrows; rows++) {
    for (int cols = 0; cols < ncols; cols++) {
      unsigned char red = img->pixels[rows*ncols+cols].r;
      unsigned char green = img->pixels[rows*ncols+cols].g;
      unsigned char blue = img->pixels[rows*ncols+cols].b;
      double double_red = red / 255.0 - 0.5;
      double double_green = green /255.0 - 0.5;
      double double_blue = blue / 255.0 - 0.5;
      double_red = double_red * mult;
      double_green = double_green * mult;
      double_blue = double_blue * mult;
      double_red = 255.0 * (double_red + 0.5);
      double_blue = 255.0 * (double_blue + 0.5);
      double_green = 255.0 * (double_green + 0.5);
      double_red = saturation(double_red);
      double_blue = saturation(double_blue);
      double_green = saturation(double_green);
      red = (unsigned char)double_red;
      blue = (unsigned char)double_blue;
      green = (unsigned char)double_green;
      img->pixels[rows*ncols+cols].r = red;
      img->pixels[rows*ncols+cols].b = blue;
      img->pixels[rows*ncols+cols].g = green;
    }
  }
  return 0;
}

double saturation(double color) {
  if(color>255) {
    color = 255;
  }
  else if(color < 0) {
    color = 0;
  }
  return color; 
}

//crop function - emily
int crop(Image** img, int colUL, int rowUL, int colLR, int rowLR) {
  int nRows = (*img)->rows;
  int nCols = (*img)->cols;
  // Check for out of bounds crop dimensions
  if (colUL < 0 || colUL < colLR || rowUL < 0 || rowUL > rowLR || colLR > nCols || rowLR > nRows) {
    return 2;
  }
  int new_nRows = rowLR-rowUL;
  int new_nCols = colLR-colUL;
  // Allocate new image
  Image *newImg = (Image *)malloc(sizeof(Image));
  newImg->cols = new_nCols;
  newImg->rows = new_nRows;
  newImg->pixels = (Pixel *)malloc(new_nCols*new_nRows*sizeof(Pixel));
  // Store correct values in newImg
  for(int rows = 0; rows < new_nRows; rows++) {
    for(int cols = 0; cols < new_nCols; cols++) {
      newImg->pixels[rows*new_nRows + cols].r = (*img)->pixels[(rows+rowUL)*nCols + (cols+colUL)].r;
      newImg->pixels[rows*new_nRows + cols].b = (*img)->pixels[(rows+rowUL)*nCols + (cols+colUL)].b;
      newImg->pixels[rows*new_nRows + cols].g = (*img)->pixels[(rows+rowUL)*nCols + (cols+colUL)].g;
    }
  }
  free((*img)->pixels);
  free(*img);
  *img = newImg;
  return 0;
}

//grayscale function - mia
int grayscale(Image *img) {
  int ncols = img->cols;
  int nrows = img->rows;
  for (int rows = 0; rows < nrows; rows++) {
    for (int cols = 0; cols < ncols; cols++) {
      unsigned char red = img->pixels[rows*ncols+cols].r;
      unsigned char green = img->pixels[rows*ncols+cols].g;
      unsigned char blue = img->pixels[rows*ncols+cols].b;
      unsigned char grey = 0.30*((float)red) + 0.59*((float)green) + 0.11*((float)blue);
      img->pixels[rows*ncols+cols].r = grey;
      img->pixels[rows*ncols+cols].b = grey;
      img->pixels[rows*ncols+cols].g = grey;
      }
  }
  return 0;
}

