//ppmIO.c
//601.220, Spring 2018
//Starter code for midterm project - feel free to edit/add to this file

#include <stdio.h>
#include <stdlib.h>
#include "ppmIO.h"
#include "commandUtil.h"
#include "imageManip.h"
#include <string.h>
#include <assert.h>

/* read PPM formatted image from a file (assumes fp != NULL) */
Image* readPPM(FILE *fp) {
  //TODO: fill in this definition
  char header[3];
  int col = 0;
  int row = 0;
  int numColors = 0;
  int readNums = 0;

  //readNums = fscanf(fp, "#%s", header); // Scans in header
  fgets(header, sizeof(header), fp);
  //printf("test:%s\n", header);
  
  // Check for comments
  int pos = ftell(fp);
  char c[1];
  fscanf(fp, " %c ", c);
  if(*c=='#') {
    //printf("Comment found\n");
    while(strcmp(c,"\n") != 0) { 
      fscanf(fp,"%c",c); // Scan until newline is reached
    }
  }
  else{
    fseek(fp, pos, SEEK_SET); // Return to beginning of line if there is no comment
    //printf("No comment found\n");
  }
  // Read in columns, rows, and color number
  readNums = fscanf(fp,"%d %d %d", &col, &row, &numColors);
  //printf("col: %i, row: %i, numcolors: %i\n",col,row,numColors);
  
  //int readNums = fscanf(fp, "%s %d %d %d", header, &col, &row, &numColors); //Scans in header
  if (readNums!=3||strcmp(header, "P6")!=0||numColors!=255){
    printf("Wrong headers\n");
    return NULL;
  }
  while(fgetc(fp) != '\n'){};
  
  Image *img = (Image *)malloc(sizeof(Image));
  img->cols = col;
  img->rows = row;
  img->pixels = (Pixel *)malloc(img->cols * img->rows * sizeof(Pixel));
  int numPixelsRead = fread(img -> pixels, sizeof(Pixel), col*row, fp);  //Reads actual file
  //printf("pixels read: %d\n", numPixelsRead);
  //printf("%d\n",img->pixels[10].r);
  if (numPixelsRead!=col*row){
    //printf("Wrong number of pixels read\n");
    return NULL;
  }
  return img;
}




/* write PPM formatted image to a file (assumes fp != NULL and img != NULL) */
int writePPM(FILE *fp, const Image* im) {

  /* abort if either file pointer is dead or image pointer is null; indicate failure with -1 */
  if (!fp || !im) {
    return -1;
  }

  /* write tag and dimensions; colors is always 255 */
  fprintf(fp, "P6\n%d %d\n%d\n", im->cols, im->rows, 255);

  /* write pixels */
  int num_pixels_written = (int) fwrite(im->pixels, sizeof(Pixel), (im->rows) * (im->cols), fp);
  /* check if write was successful or not; indicate failure with -1 */
  if (num_pixels_written != (im->rows) * (im->cols)) {
    return -1;
  }
  //fprintf(fp, "\n");
  /* success, so return number of pixels written */
  //printf("pixels written: %d\n",num_pixels_written);
  
  return num_pixels_written;
}
