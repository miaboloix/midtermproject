#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "ppmIO.h"
#include "commandUtil.h"
#include "imageManip.h"

int driver(int argc, char **argv) {
  //opening file
  if(argc < 3) {
    printf("ERROR: Failed to supply input or output filename, or both.\n");
    return 1;
  }
  FILE *read = fopen(argv[1],"rb"); //open file
  if (read == NULL) { //check if file was read
    printf("ERROR: File couldn't be found.\n");
    return 2;
  }

  //Reading in file using readPPM() function

  Image *img = readPPM(read); //read file
  if (img == NULL) { //check if file was read
    printf("ERROR: Image couldn't be read.\n");
    return 3;
  }

  //Closing file
  fclose(read);

  if(argc == 3) {
    printf("ERROR: No operation specified.\n");
    return 5;
  }

  //Determine which operation to do using strcmp() function (string compare)
  //
  int incorrectArguments = 1;
  // argv[3] is the third element of argv, AKA first word of user input where the name of the function they would like to run is expected
  //
  char *operation = argv[3];
  int ret = 0;
  if(strcmp(operation,"swap")==0) {
    if(argc == 4) {
      // SWAP
      // printf("Execute swap operation.\n");
      ret = swap(img);
      incorrectArguments = 0;
    }
  }
  else if(strcmp(operation,"blackout")==0) {
    if(argc == 4) {
      //BLACKOUT
      //printf("Execute blackout operation. \n");
      ret = blackout(img);
      incorrectArguments = 0;
    }
  }
  else if(strcmp(operation,"crop")==0) {
    if(argc == 8) {
	    //CROP
      ret = crop(&img,atoi(argv[4]),atoi(argv[5]),atoi(argv[6]),atoi(argv[7]));
      //printf("Execute crop operation. \n");
      incorrectArguments = 0;
    }
  }
  else if(strcmp(operation,"grayscale")==0) {
    if(argc == 4) {
	    //GRAYSCALE
      //printf("Execute grayscale operation. \n");
      ret = grayscale(img);
      incorrectArguments = 0;
    }
  }
  else if(strcmp(operation,"contrast")==0) {
    if(argc == 5) {
	    //CONTRAST
      //printf("Execute contrast operation. \n");
      //printf("%0.3f\n",double(*argv[4]));
      ret = contrast(img, atof(argv[4]));
      incorrectArguments = 0;
    }
  }
  else {
    printf("ERROR: Invalid operation\n");
    return 5;
  }
  if(incorrectArguments == 1) {
    printf("ERROR: Incorrect number of arguments\n");
    return 6;
  }
  if(ret != 0 && ret != 2) {
    printf("ERROR: Operation failed\n");
    return 8;
  }
  else if(ret == 2) {
    printf("ERROR: Crop dimensions out of range.\n");
    return 7;
  }
  
  // Save the name of the ppm file in which the new edited pic will be saved
  //
  char *output_name = argv[2];
  //Write in the file
  //
  FILE *file = fopen(output_name,"wb"); // update to save output by correct name
  if (!file) {
    printf("ERROR: Could not open output file.\n");
    return 4;
  }
  
  //Write using writePPM() function
  ret = writePPM(file, img);
  if (ret == -1) {
    printf("ERROR: Writing output failed.\n");
    return 4;
  }
  
  //Close writeen file
  fclose(file);
  free(img->pixels);
  free(img);
  //Return 0 if program ran as expected!
  //
  //
  return 0; 
}
