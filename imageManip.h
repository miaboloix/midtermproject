#ifndef IMAGEMANIP_H
#define IMAGEMANIP_H

int blackout(Image* img); 
int crop(Image** img, int colUL, int rowUL, int colLR, int rowLR);
int swap(Image* img);
int grayscale(Image* img);
int contrast(Image* img, double mult);
double saturation(double color);
#endif
