# Lines starting with # are comments

# Some variable definitions to save typing later on
CC = gcc
CFLAGS = -std=c99 -Wall -Wextra -pedantic

project: project.o ppmIO.o commandUtil.o imageManip.o
	$(CC) $(CFLAGS) -o project project.o ppmIO.o commandUtil.o imageManip.o

# Compiles project.c into an object file
project.o: project.c
	$(CC) $(CFLAGS) -c project.c

# Compiles ppmIO.c into an object file
ppmIO.o: ppmIO.c ppmIO.h
	$(CC) $(CFLAGS) -c ppmIO.c

# Compiles commandUtil.c into an object file
commandUtil.o: commandUtil.c commandUtil.h
	$(CC) $(CFLAGS) -c commandUtil.c

# Compiles imageManip.c into an object file
imageManip.o: imageManip.c imageManip.h
	$(CC) $(CFLAGS) -c imageManip.c

# Links project
# project: project.o ppmIO.o commandUtil.o #imageManip.o

# 'make clean' will remove intermediate & executable files
clean:
	rm -f *.o
